python app/dblib/db_setup.py

chown www-data:www-data gtp.db

python app/adminlib/default_admin.py

read -n 1 -p "Press the ENTER key once you've recorded the admin details"

rm app/adminlib/default_admin.py

mkdir sessions/ ssl/

openssl genrsa -out ssl/gtp-key.pem 2048
openssl req -new -x509 -days 1095 -key ssl/gtp-key.pem -out ssl/gtp.pem

rm -- "$0"

exit
