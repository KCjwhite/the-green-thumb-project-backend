#Copyright 2018, 2019 Joshua White
#This file is part of The Greenthumb Project Backend Server.

#The Greenthumb Project Backend Server is free software: you can redistribute 
#it and/or modify it under the terms of the GNU General Public License as 
#published by the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The Greenthumb Project Backend Server is distributed in the hope that it will
#be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
#Public License for more details.

#You should have received a copy of the GNU General Public License
#along with The Greenthumb Project Backend Server.  If not,
#see <https://www.gnu.org/licenses/>.

# Comment this section when testing locally
if __name__ == "__main__":
    import sys
    sys.path.insert(0, '/builds/KCjwhite/the-green-thumb-project-backend/gtp/gtp-volume')

# Uncomment this section when testing on your local system
#if __name__ == "__main__":
#    import sys
#    sys.path.insert(0, '/usr/local/gtp')

import app.dblib.db_setup as ds
import app.dblib.db_interact as di
import json, os, datetime

### This is a quick test to verify the CRUD functions on a brand new db.
### This test is for dev only and shouldn't run on an existing db.

### Create temp database
print("\n")
print("Creating test database")
ds.Base.metadata.create_all(ds.engine)

### Create all temp items
print("\n")
try:
    print("Creating test Greenhouse")
    json_greenhouse = json.loads('{"name": "test"}')
    di.create_greenhouse(json_greenhouse)
    print("Done")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem making a Greenhouse"

try:
    print("Creating test Sensor")
    json_sensor = json.loads('{"name": "test", "greenhouse_id": "1"}')
    di.create_sensor(json_sensor)
    print("Done")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem making Sensor"

try:
    print("Creating test Reading")
    json_reading = json.loads('{"sensor_id": "1", "reading": "test"}')
    di.create_reading(json_reading)
    print("Done")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem making Reading"

try:
    print("Creating test User")
    json_user = json.loads('{"username": "test", "password": "test"}')
    di.create_user(json_user)
    print("Done")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem making User"

### Test updating all item types
print("\n")
try:
    print("Updating test Greenhouse")
    json_greenhouse = json.loads('{"name": "test2"}')
    di.update_item("greenhouses", "1", json_greenhouse)
    print("Done")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem updating Greenhouse"

try:
    print("Updating test Sensor")
    json_sensor = json.loads('{"name": "test2", "greenhouse_id": "1"}')
    di.update_item("sensors", "1", json_sensor)
    print("Done")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem updating Sensor"

try:
    print("Updating test Reading")
    json_reading = json.loads('{"sensor_id": "1", "reading": "test2"}')
    di.update_item("readings", "1", json_reading)
    print("Done")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem updating Reading"

try:
    print("Updating test User")
    json_user = json.loads('{"username": "test2", "password": "test"}')
    di.update_item("users", "1", json_user)
    print("Done")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem updating User"


### Test fetching all item types
print("\n")
try:
    print("Fetching created test items")
    greenhouses = di.fetch_all("greenhouses")
    assert (greenhouses[0].name == json_greenhouse["name"])
    print("Greenhouse worked")
    
    sensors = di.fetch_all("sensors")
    assert (sensors[0].name == json_sensor["name"])
    print("Sensor worked")

    readings = di.fetch_all("readings")
    assert (readings[0].reading == json_reading["reading"])
    print("Reading worked")

    time_readings = di.fetch_timeframe((datetime.datetime.now() -
            datetime.timedelta(minutes = 10)).timestamp())
    assert (time_readings[0].reading == json_reading["reading"])
    print("Reading based on time worked")
    
    users = di.fetch_all("users")
    assert (users[0].username == json_user["username"])
    print("User worked")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem fetching items"

### Test delete item function
print("\n")
try:
    di.delete_item("readings", "1")
    print("Reading deleted")
    di.delete_item("sensors", "1")
    print("Sensor deleted")
    di.delete_item("greenhouses", "1")
    print("Greenhouse deleted")
    di.delete_item("users", "1")
    print("User deleted")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "There was a problem deleting the last item"

print("\n")
print("Removing test database.\n")
os.remove(os.path.join(os.path.dirname(os.path.dirname(
          os.path.abspath(__file__))), "gtp.db"))
print("All tests passed.  Current models in database:")
print([key for key in di.fetch_models().keys()])
