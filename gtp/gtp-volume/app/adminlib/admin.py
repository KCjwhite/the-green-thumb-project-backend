#Copyright 2018, 2019 Joshua White
#This file is part of The Greenthumb Project Backend Server.

#The Greenthumb Project Backend Server is free software: you can redistribute 
#it and/or modify it under the terms of the GNU General Public License as 
#published by the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The Greenthumb Project Backend Server is distributed in the hope that it will
#be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
#Public License for more details.

#You should have received a copy of the GNU General Public License
#along with The Greenthumb Project Backend Server.  If not,
#see <https://www.gnu.org/licenses/>.

from string import ascii_letters, digits
from random import randint, choice
from passlib.hash import pbkdf2_sha256

### Random token generator
def token_generator(chars=ascii_letters + digits):
    return ''.join(choice(chars) for x in range(randint(16,20)))

### Make hash from password for db storage
def pass2hash(password):
    return pbkdf2_sha256.hash(password)

#### Hash supplied password to verify against db
def pass_verify(password, hash):
    return pbkdf2_sha256.verify(password, hash)

#Change error pages to only show HTTP information instead of code output.
def error_page(status, message, traceback, version):
    return f"{status}"
