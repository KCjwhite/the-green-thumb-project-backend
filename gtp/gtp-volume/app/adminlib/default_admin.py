if __name__ == "__main__":
    import sys
    sys.path.insert(0, '/usr/local/gtp')

import app.dblib.db_setup as ds
import app.dblib.db_interact as di
import app.adminlib.admin as a
import json

password = a.token_generator()
default_admin = f'{{ "username": "admin", "password": "{password}" }}'
print(default_admin)
json_user = json.loads(default_admin)
di.create_user(json_user)

print("\n***PLEASE KEEP THIS PASSWORD IN A SAFE PLACE***")
print("***THERE IS NO WAY TO RECOVER THIS PASSWORD IF LOST***")
print("Username = admin, Password = " + password + "\n")

