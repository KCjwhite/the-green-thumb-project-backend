#Copyright 2018, 2019 Joshua White
#This file is part of The Greenthumb Project Backend Server.

#The Greenthumb Project Backend Server is free software: you can redistribute 
#it and/or modify it under the terms of the GNU General Public License as 
#published by the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The Greenthumb Project Backend Server is distributed in the hope that it will
#be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
#Public License for more details.

#You should have received a copy of the GNU General Public License
#along with The Greenthumb Project Backend Server.  If not,
#see <https://www.gnu.org/licenses/>.

from app.adminlib.admin import pass_verify
from app.dblib.db_interact import session
from app.dblib.db_setup import User, Sensor
import cherrypy

### Check has the user supplied credentials against database hash
def check_user(supplied_creds):
    try:
        user = session.query(User).filter(User.username == supplied_creds['username']).first()
        return pass_verify(supplied_creds['password'], user.password)
    except:
        raise cherrypy.HTTPError(401)

### Check the sensor suppied token again all tokens in database
def check_token(supplied_id, supplied_token):
    stored = session.query(Sensor).filter(Sensor.id == supplied_id).first()
    assert (supplied_token == stored.token)

### Check if user's session is active
def check_session(self):
    try:
        assert (cherrypy.request.cookie['session_id'].value == cherrypy.session.id)
    except:
        raise cherrypy.HTTPRedirect("/")


