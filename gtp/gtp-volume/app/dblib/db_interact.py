#Copyright 2018, 2019 Joshua White
#This file is part of The Greenthumb Project Backend Server.

#The Greenthumb Project Backend Server is free software: you can redistribute 
#it and/or modify it under the terms of the GNU General Public License as 
#published by the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The Greenthumb Project Backend Server is distributed in the hope that it will
#be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
#Public License for more details.

#You should have received a copy of the GNU General Public License
#along with The Greenthumb Project Backend Server.  If not,
#see <https://www.gnu.org/licenses/>.

from sqlalchemy.orm import sessionmaker

from app.dblib.db_setup import engine, Base, Greenhouse, Sensor, Reading, User
from app.adminlib.admin import pass_verify

Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)

session = DBSession()

#Pulls all object classes based on table names.  SQLAlchmey the class itself
#for its various ORM functions.
def fetch_models():
    all_models = {}
    for model in Base._decl_class_registry.values():
        if hasattr(model, '__tablename__'):
            all_models[model.__tablename__] = model
    return all_models

#Get all sensor tokens to authentication readings getting sent
def fetch_tokens():
    return session.query(Sensor.token).all()

#Pulls all entries/records related to object class (eq. all greenhouses).
def fetch_all(item):
    all_models = fetch_models()
    items = session.query(all_models[item]).all()
    return items

#Pulls all sensor readings for the given time frame measured in epoch time.
def fetch_timeframe(time):
    items = session.query(Reading).filter(Reading.time >= time).all()
    return items

def create_greenhouse(json_body):
    newgreen = Greenhouse(name=json_body['name'])
    session.add(newgreen)
    session.commit()

def create_sensor(json_body):
    newsensor = Sensor(name=json_body['name'], greenhouse_id=json_body['greenhouse_id'])
    session.add(newsensor)
    session.commit()

def create_reading(json_body):
    newreading = Reading(sensor_id=json_body['sensor_id'], reading=json_body['reading'])
    session.add(newreading)
    session.commit()

def create_user(json_body):
    newuser = User(username=json_body['username'], password=json_body['password'])
    session.add(newuser)
    session.commit()

#Uses object class (item), object id (item_id) supplied in the HTTP URN to find
#the entry/record, then updates the entry/record with the info in json_body. 
def update_item(item, item_id, json_body):
    all_models = fetch_models()
    toupdate = session.query(all_models[item]).filter(all_models[item].id == item_id).update(json_body)
    session.commit()

#Uses object class (item), object id (item_id) supplied in the HTTP URN to find
#the entry/record, then deletes that entry.
def delete_item(item, item_id):
    all_models = fetch_models()
    todelete = session.query(all_models[item]).filter(all_models[item].id == item_id).delete()
    session.commit()

