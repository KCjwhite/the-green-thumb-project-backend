//Copyright 2018, 2019 Joshua White
//This file is part of The Greenthumb Project Backend Server.
//
//The Greenthumb Project Backend Server is free software: you can redistribute 
//it and/or modify it under the terms of the GNU General Public License as 
//published by the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//The Greenthumb Project Backend Server is distributed in the hope that it will
//be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
//Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with The Greenthumb Project Backend Server.  If not,
//see <https://www.gnu.org/licenses/>.

const pageWelcome = {
  template: `
    <div class="w3-container w3-center">
      <h1>Thank you for using The Green Thumb Project!</h1>
      <p>If you would like to contribute to the project please feel free to contact us or fork the project.  We would love to have additional cool and useful features, translations to different languages, or your creative idea!</p>
    </div>
  `
}

const pageGreenhouses = {
  data () {
    return {
      greenhouses: null,
      message: null,
      id: null,
      name: null,
      updateID: null,
      updateName: null
    }
  },
  mounted () {
    this.getGreenhouses();
  },
  methods: {
    getGreenhouses: function () {
      window.axios
      .get('/admin/greenhouses')
      .then(response => (this.greenhouses = response.data))
      .catch(error => (this.greenhouses = "Couldn't get greenhouses."))
    },
    makeGreenhouse: function () {
      window.axios
        .post('/admin/greenhouses', {"name": this.name})
        .then(response => (this.message = response.data,
          this.getGreenhouses()))
        .catch(error => (this.message = "Problem adding."))
    },
    updateGreenhouse: function () {
      window.axios
        .patch('/admin/greenhouses/' + this.updateID, {"name": this.updateName})
        .then(response => (this.message = response.data,
          this.getGreenhouses()))
        .catch(error => (this.message = "Problem updating."))
    },
    deleteGreenhouse: function (id) {
      window.axios
        .delete('/admin/greenhouses/' + id)
        .then(response => (this.message = response.data,
          this.getGreenhouses()))
        .catch(error => (this.message = "Problem deleting."))
    }
  },
  template: `
  <div class="w3-container w3-center">
    <table class="w3-auto w3-padding-large" style="min-width:50%">
      <tr>
        <th>ID</th>
        <th>Name</th>
      </tr>
      <tr v-for="(greenhouse, index) in greenhouses">
        <td
          v-if="!(index % 2)"
          class="w3-center w3-padding">{{ greenhouse.id }}</td>
        <td
          v-if="!(index % 2)"
          class="w3-center w3-padding">{{ greenhouse.name }}</td>
        <td
          v-if="(index % 2)"
          class="w3-center w3-padding w3-gray">{{ greenhouse.id }}</td>
        <td
          v-if="(index % 2)"
          class="w3-center w3-padding w3-gray">{{ greenhouse.name }}</td>
      </tr>
    </table>
    <h3 class="w3-row">{{ message }}</h3>
    <div class="w3-row">
      <span>Add a new greenhouse - </span>
      <input 
        type='text' 
        v-model="name" 
        placeholder="Greenhouse name" 
        class="w3-padding"></input>
      <div 
        @click="makeGreenhouse" 
        class="w3-green w3-padding w3-button">Create</div>
    </div>
    <br>
    <div class="w3-row">
      <span>Edit greenhouse - </span>
      <input 
        type='text' 
        v-model="updateID" 
        placeholder="Greenhouse ID" 
        class="w3-padding"></input>
      <input
        type='text' 
        v-model="updateName" 
        placeholder="New greenhouse name" 
        class="w3-padding"></input>
      <div
        @click="updateGreenhouse" 
        class="w3-blue w3-padding w3-button">Update</div>
    </div>
    <br>
    <div class="w3-row">
      <span>Delete a greenhouse - </span>
      <input
        type='text' 
        v-model="id" 
        placeholder="Greenhouse ID" 
        class="w3-padding"></input>
      <div 
        @click="deleteGreenhouse(id)" 
        class="w3-red w3-padding w3-button">Delete</div>
    </div>
  </div>
  `
}

const pageSensors = {
  data () {
    return {
      sensors: null,
      message: null,
      id: null,
      name: null,
      greenhouseID: null,
      updateID: null,
      updateName: null,
      updateGreenhouseID: null
    }
  },
  mounted () {
    this.getSensors();
  },
  methods: {
    getSensors: function () {
      window.axios
      .get('/admin/sensors')
      .then(response => (this.sensors = response.data))
      .catch(error => (this.sensors = "Couldn't get sensors."))
    },
    makeSensor: function () {
      window.axios
        .post('/admin/sensors', {"name": this.name, 
          "greenhouse_id": this.greenhouseID})
        .then(response => (this.message = response.data,
          this.getSensors()))
        .catch(error => (this.message = "Problem adding."))
    },
    updateSensor: function () {
      window.axios
        .patch('/admin/sensors/' + this.updateID, {"name": this.updateName,
          "greenhouse_id": this.updateGreenhouseID})
        .then(response => (this.message = response.data,
          this.getSensors()))
        .catch(error => (this.message = "Problem updating."))
    },
    deleteSensor: function (id) {
      window.axios
        .delete('/admin/sensors/' + id)
        .then(response => (this.message = response.data,
          this.getSensors()))
        .catch(error => (this.message = "Problem deleting."))
    }
  },
  template: `
  <div class="w3-container w3-center">
    <table class="w3-auto w3-padding-large" style="min-width:50%">
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Token</th>
        <th>Greenhouse ID</th>
        <th>Greenhouse Name</th>
      </tr>
      <tr v-for="(sensor, index) in sensors">
        <td
          v-if="!(index % 2)"
          class="w3-center w3-padding">{{ sensor.id }}</td>
        <td
          v-if="!(index % 2)"
          class="w3-center w3-padding">{{ sensor.name }}</td>
        <td
          v-if="!(index % 2)"
          class="w3-center w3-padding">{{ sensor.token }}</td>
        <td
          v-if="!(index % 2)"
          class="w3-center w3-padding">{{ sensor.greenhouse_id }}</td>
        <td
          v-if="!(index % 2)"
          class="w3-center w3-padding">{{ sensor.greenhouse_name }}</td>
        <td
          v-if="(index % 2)"
          class="w3-center w3-padding w3-gray">{{ sensor.id }}</td>
        <td
          v-if="(index % 2)"
          class="w3-center w3-padding w3-gray">{{ sensor.name }}</td>
        <td
          v-if="(index % 2)"
          class="w3-center w3-padding w3-gray">{{ sensor.token }}</td>
        <td
          v-if="(index % 2)"
          class="w3-center w3-padding w3-gray">{{ sensor.greenhouse_id }}</td>
        <td
          v-if="(index % 2)"
          class="w3-center w3-padding w3-gray">{{ sensor.greenhouse_name }}</td>
      </tr>
    </table>
    <h3 class="w3-row">{{ message }}</h3>
    <div class="w3-row">
      <span>Add a new sensor - </span>
      <input 
        type='text' 
        v-model="name" 
        placeholder="Sensor name" 
        class="w3-padding"></input>
      <input 
        type='text' 
        v-model="greenhouseID" 
        placeholder="Greenhouse ID" 
        class="w3-padding"></input>
      <div 
        @click="makeSensor" 
        class="w3-green w3-padding w3-button">Create</div>
    </div>
    <p class="w3-center w3-text-red">All update fields need to be completed.</p>
    <div class="w3-row">
      <span>Edit sensor - </span>
      <input 
        type='text' 
        v-model="updateID" 
        placeholder="Sensor ID" 
        class="w3-padding"></input>
      <input 
        type='text' 
        v-model="updateName" 
        placeholder="New sensor name" 
        class="w3-padding"></input>
      <input
        type='text'
        v-model="updateGreenhouseID"
        placeholder="Greenhouse ID"
        class="w3-padding"></input>
      <div 
        @click="updateSensor" 
        class="w3-blue w3-padding w3-button">Update</div>
    </div>
    <br>
    <div class="w3-row">
      <span>Delete a greenhouse - </span>
      <input 
        type='text' 
        v-model="id" 
        placeholder="Sensor ID" 
        class="w3-padding"></input>
      <div 
        @click="deleteSensor(id)" 
        class="w3-red w3-padding w3-button">Delete</div>
    </div>
  </div>
  `
}

const pageUsers = {
  data () {
    return {
      users: null,
      message: null,
      id: null,
      username: null,
      password: null,
      updateID: null,
      updateUsername: null,
      updatePassword: null
    }
  },
  mounted () {
    this.getUsers();
  },
  methods: {
    getUsers: function () {
      window.axios
      .get('/admin/users')
      .then(response => (this.users = response.data))
      .catch(error => (this.users = "Couldn't get users."))
    },
    makeUser: function () {
      window.axios
        .post('/admin/users', {"username": this.username,
          "password": this.password})
        .then(response => (this.message = response.data,
          this.getUsers()))
        .catch(error => (this.message = "Problem adding."))
    },
    updateUser: function () {
      window.axios
        .patch('/admin/users/' + this.updateID, {"username": this.updateUsername,
          "password": this.updatePassword})
        .then(response => (this.message = response.data,
          this.getUsers()))
        .catch(error => (this.message = "Problem updating."))
    },
    deleteUser: function (id) {
      window.axios
        .delete('/admin/users/' + id)
        .then(response => (this.message = response.data,
          this.getUsers()))
        .catch(error => (this.message = "Problem deleting."))
    }
  },
  template: `
  <div class="w3-container w3-center">
    <table class="w3-auto w3-padding-large" style="min-width:50%">
      <tr>
        <th>ID</th>
        <th>Name</th>
      </tr>
      <tr v-for="(user, index) in users">
        <td
          v-if="!(index % 2)"
          class="w3-center w3-padding">{{ user.id }}</td>
        <td
          v-if="!(index % 2)"
          class="w3-center w3-padding">{{ user.username }}</td>
        <td
          v-if="(index % 2)"
          class="w3-center w3-padding w3-gray">{{ user.id }}</td>
        <td
          v-if="(index % 2)"
          class="w3-center w3-padding w3-gray">{{ user.username }}</td>
      </tr>
    </table>
    <h3 class="w3-row">{{ message }}</h3>
    <div class="w3-row">
      <span>Add a new user - </span>
      <input 
        type='text' 
        v-model="username" 
        placeholder="Username" class="w3-padding"></input>
      <input 
        type='password' 
        v-model="password"
        placeholder="Password" class="w3-padding"></input>
      <div @click="makeUser" class="w3-green w3-padding w3-button">Create</div>
    </div>
    <p class="w3-center w3-text-red">All update fields need to be completed.</p>
    <div class="w3-row">
      <span>Edit user - </span>
      <input 
        type='text' 
        v-model="updateID" 
        placeholder="User ID" 
        class="w3-padding"></input>
      <input 
        type='text'
        v-model="updateUsername"
        placeholder="New username"
        class="w3-padding"></input>
      <input
        type='password'
        v-model="updatePassword"
        placeholder="Password" class="w3-padding"></input>
      <div
        @click="updateUser"
        class="w3-blue w3-padding w3-button">Update</div>
    </div>
    <br>
    <div class="w3-row">
      <span>Delete a user - </span>
      <input 
        type='text'
        v-model="id"
        placeholder="User ID"
        class="w3-padding"></input>
      <div
        @click="deleteUser(id)"
        class="w3-red w3-padding w3-button">Delete</div>
    </div>
  </div>
  `
}
  

const router = new VueRouter({
  routes: [
    { path: '/', component: pageWelcome },
    { path: '/greenhouses', component: pageGreenhouses },
    { path: '/sensors', component: pageSensors },
    { path: '/users', component: pageUsers }
  ]
})

const admin = new Vue({
  router
}).$mount('#admin')

var test = {
  data: {
    readings: null,
    seconds: null
  },
  mounted () {
  },
  methods: {
    getItems: function (item) {
      window.axios
      .get('/admin/' + item)
      .then(response => (this.readings = response.data))
      .catch(error => (this.readings = "Couldn't get readings."))
    }
  }
}
